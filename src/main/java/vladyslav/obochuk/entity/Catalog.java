package vladyslav.obochuk.entity;

import vladyslav.obochuk.xml.XMLStructure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = XMLStructure.ROOT_ELEM)
@XmlAccessorType(XmlAccessType.FIELD)
public class Catalog {
    @XmlElement(name = XMLStructure.PERSON_ELEM)
    private List<Person> people;

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }
}
