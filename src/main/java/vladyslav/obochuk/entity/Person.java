package vladyslav.obochuk.entity;

import vladyslav.obochuk.xml.XMLStructure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

/**
 * Created by Vladyslav_Obochuk on 6/8/2018.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Person {

    @XmlAttribute(name = XMLStructure.PERSON_ATTR)
    private Long id;

    @XmlElement(name = XMLStructure.PERSON_NAME)
    private String name;

    @XmlElement(name = XMLStructure.PERSON_ADDRESS)
    private String address;

    @XmlElement(name = XMLStructure.PERSON_CASH)
    private Long cash;

    @XmlElement(name = XMLStructure.PERSON_EDUCATION)
    private String education;

    public Person(){}

    public Person(Long id, String name, String address, Long cash, String education) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.cash = cash;
        this.education = education;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getCash() {
        return cash;
    }

    public void setCash(Long cash) {
        this.cash = cash;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }
}
