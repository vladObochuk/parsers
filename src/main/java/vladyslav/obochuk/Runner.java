package vladyslav.obochuk;

import vladyslav.obochuk.entity.Person;
import vladyslav.obochuk.xml.builder.XMLBuilder;
import vladyslav.obochuk.xml.parser.DOMXMLParser;
import vladyslav.obochuk.xml.parser.JAXBXMLParser;
import vladyslav.obochuk.xml.parser.XMLParser;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Vladyslav_Obochuk on 6/8/2018.
 */
public class Runner {

    public static void run() throws Exception{
        /*XMLBuilder builder = new XMLBuilder("catalog.xml");
        List<Person> persons = Arrays.asList(
                new Person(1L, "Vladyslav Obochuk", "Kyiv", 500L, "bachelor"),
                new Person(2L, "Roman Karaulnyi", "Kyiv", 200L, "none"),
                new Person(3L, "Nadiia Tkachenko", "Kuiv", 1500L, "dom")
        );

        builder.writeEntities(persons);*/

        XMLParser parser = new JAXBXMLParser("catalog.xml");
        System.out.println(parser.parse());
    }

}
