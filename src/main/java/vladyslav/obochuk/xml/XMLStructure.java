package vladyslav.obochuk.xml;

/**
 * Created by Vladyslav_Obochuk on 6/10/2018.
 */
public interface XMLStructure {

    String ROOT_ELEM = "catalog";

    String PERSON_ELEM = "person";
    String PERSON_ATTR = "id";
    String PERSON_NAME = "name";
    String PERSON_ADDRESS = "address";
    String PERSON_CASH = "cash";
    String PERSON_EDUCATION = "education";

}
