package vladyslav.obochuk.xml.builder;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import vladyslav.obochuk.entity.Person;
import vladyslav.obochuk.xml.XMLStructure;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

/**
 * Created by Vladyslav_Obochuk on 6/8/2018.
 */
public class XMLBuilder {
    private String xmlFileName;

    public XMLBuilder(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void writeEntities(List<Person> persons) throws Exception {
        Document document = createDocument();
        Element rootElement = document.createElement(XMLStructure.ROOT_ELEM);
        document.appendChild(rootElement);

        for (Person person : persons) {
            Element personElement = addPersonElement(document, person);
            rootElement.appendChild(personElement);
        }

        writeDocument(document);

    }

    private Document createDocument() throws Exception {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        return docBuilder.newDocument();
    }


    private Element addPersonElement(Document document, Person person) {
        Element personElement = document.createElement(XMLStructure.PERSON_ELEM);

        Attr idAttr = addAttributeWithValue(document, XMLStructure.PERSON_ATTR, person.getId().toString());
        personElement.setAttributeNode(idAttr);

        Element name = addElementWithValue(document, XMLStructure.PERSON_NAME, person.getName());
        personElement.appendChild(name);

        Element address = addElementWithValue(document, XMLStructure.PERSON_ADDRESS, person.getAddress());
        personElement.appendChild(address);

        Element cash = addElementWithValue(document, XMLStructure.PERSON_CASH, person.getCash().toString());
        personElement.appendChild(cash);

        Element education = addElementWithValue(document, XMLStructure.PERSON_EDUCATION, person.getEducation());
        personElement.appendChild(education);

        return personElement;
    }

    private Element addElementWithValue(Document document, String name, String value) {
        Element newElement = document.createElement(name);
        newElement.appendChild(document.createTextNode(value));
        return newElement;
    }

    private Attr addAttributeWithValue(Document document, String name, String value) {
        Attr attr = document.createAttribute(name);
        attr.setValue(value);
        return attr;
    }

    private void writeDocument(Document document) throws Exception {
        Transformer transformer = createTransformer();

        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(new File(xmlFileName));

        transformer.transform(source, result);
    }

    private Transformer createTransformer() throws Exception {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        return transformer;
    }
}
