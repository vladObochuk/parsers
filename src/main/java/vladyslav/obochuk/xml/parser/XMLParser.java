package vladyslav.obochuk.xml.parser;

import vladyslav.obochuk.entity.Person;

import java.util.List;

/**
 * Created by Vladyslav_Obochuk on 6/10/2018.
 */
public interface XMLParser {
    List<Person> parse() throws Exception;
}
