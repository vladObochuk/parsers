package vladyslav.obochuk.xml.parser;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import vladyslav.obochuk.entity.Person;
import vladyslav.obochuk.xml.XMLStructure;

import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vladyslav_Obochuk on 6/8/2018.
 */
public class DOMXMLParser implements XMLParser {

    private String xmlFileName;

    public DOMXMLParser(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public List<Person> parse() throws Exception {
        Document document = createDocument();
        List<Person> people = new ArrayList<>();

        NodeList peopleList = document.getElementsByTagName(XMLStructure.PERSON_ELEM);
        for (int i = 0; i < peopleList.getLength(); i++) {
            Element personElem = (Element) peopleList.item(i);
            Person person = retrievePerson(personElem);
            people.add(person);
        }
        return people;
    }

    private Document createDocument() throws Exception{
        return DocumentBuilderFactory.newInstance()
                .newDocumentBuilder()
                .parse(xmlFileName);
    }

    private Person retrievePerson(Element personElement){
        NodeList personElemChild = personElement.getChildNodes();
        Person person = new Person();
        person.setId(Long.parseLong(personElement.getAttribute(XMLStructure.PERSON_ATTR)));
        for (int i = 0; i < personElemChild.getLength(); i++) {
            Node childNode = personElemChild.item(i);
            setValueFromNode(childNode, person);
        }
        return person;
    }

    private void setValueFromNode(Node node, Person person){
        switch (node.getNodeName()){
            case XMLStructure.PERSON_ADDRESS:
                person.setAddress(node.getFirstChild().getNodeValue());
                break;
            case XMLStructure.PERSON_CASH:
                person.setCash(Long.parseLong(node.getFirstChild().getNodeValue()));
                break;
            case XMLStructure.PERSON_EDUCATION:
                person.setEducation(node.getFirstChild().getNodeValue());
                break;
            case XMLStructure.PERSON_NAME:
                person.setName(node.getFirstChild().getNodeValue());
                break;
        }
    }
}
