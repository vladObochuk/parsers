package vladyslav.obochuk.xml.parser;

import vladyslav.obochuk.entity.Catalog;
import vladyslav.obochuk.entity.Person;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

/**
 * Created by Vladyslav_Obochuk on 6/10/2018.
 */
public class JAXBXMLParser implements XMLParser{

    private String xmlFileName;

    public JAXBXMLParser(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    @Override
    public List<Person> parse() throws Exception {
        JAXBContext jaxbContext = JAXBContext.newInstance(Catalog.class);

        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        Catalog catalog = (Catalog) unmarshaller.unmarshal(new File(xmlFileName));
        return catalog.getPeople();
    }
}
